# This file is part of Sasuga.

# Sasuga is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Sasuga is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Sasuga.  If not, see <http://www.gnu.org/licenses/>.

META=install.rdf chrome.manifest COPYING
CONTENT=content/overlay.xul content/window.xul content/window.js
LOCALE=locale/en-US/translations.dtd locale/en-US/strings.properties

sasuga.xpi: $(META) $(CONTENT) $(LOCALE)
	zip -r $@ $^

.PHONY: clean

clean:
	@rm -f sasuga.xpi
