// This file is part of Sasuga.

// Sasuga is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Sasuga is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Sasuga.  If not, see <http://www.gnu.org/licenses/>.

function initWithParams(params) {
    Sasuga.fillInput(params.host);
}

var Sasuga = (function () {
    'use strict';
    let Cc = Components.classes;
    let Ci = Components.interfaces;
    let Cu = Components.utils;

    Cu.import('resource://gre/modules/Services.jsm');

    let Strings = document.getElementById('bundle_sasuga');

    let o = {};

    let textboxes = 0;
    let branch = Services.prefs.getBranch('general.useragent.override.');

    let prefs = branch.getChildList('');
    let userprefs = [];
    let treebox = null;

    for (let p in prefs) {
	if (branch.prefHasUserValue(prefs[p]) == true) {
	    userprefs.push({
		key: prefs[p],
		value: branch.getCharPref(prefs[p])
	    });
	}
    }

    o.TreeView = {
	get rowCount() {
	    return userprefs.length;
	},

	getCellText: function (row, column) {
	    switch (column.id) {
	    case 'sasuga-domain-column':
		return userprefs[row].key;
	    case 'sasuga-ua-column':
		return userprefs[row].value;
	    default:
		throw Strings.getFormattedString('invalidId', [column.id]);
	    }
	},

	setTree: function (tree) {
	    treebox = tree;
	},

	isContainer: function () {
	    return false;
	},

	isSeparator: function () {
	    return false;
	},

	isSorted: function () {
	    return false;
	},

	getLevel: function (row) {
	    return 0;
	},

	getImageSrc: function (row, column) {
	    return null;
	},

	getRowProperties: function (row) {
	    return;
	},

	getCellProperties: function (row, columns) {
	    return;
	},

	getColumnProperties: function (column) {
	    return;
	},

	cycleHeader: function (column) {
	    return;
	},
    };

    let treeHasRow = function (key) {
	for (let i=0; i<userprefs.length; ++i) {
	    if (userprefs[i].key === key) {
		return i;
	    }
	}

	return -1;
    };

    let addRow = function (key, value) {
	let index = treeHasRow(key);
	if (index >= 0) {
	    userprefs[index].value = value;
	    return -1;
	} else {
	    userprefs.push({
		key: key,
		value: value,
	    });
	}

	return userprefs.length-1;
    };

    let removeRow = function (index) {
	userprefs.splice(index, 1);
    };

    o.operate = function () {
	let domain = document.getElementById('sasuga-domain-input').value;
	if (typeof domain !== 'string' || domain.length == 0) {
	    Services.prompt.alert(window, Strings.getString('error'),
				  Strings.getString('invalidInput'));
	    throw Strings.getString('invalidInput');
	}

	let override = document.getElementById('sasuga-ua-input').value;
	if (typeof override !== 'string' || override.length == 0) {
	    Services.prompt.alert(window, Strings.getString('error'),
				  Strings.getString('invalidInput'));
	    throw Strings.getString('invalidInput');
	}

	branch.setCharPref(domain, override);

	// Services.prompt.alert(window, Strings.getString('success'),
	// 		      Strings.getString('done'));

	document.getElementById('sasuga-domain-input').value = '';
	document.getElementById('sasuga-ua-input').value = '';
	document.getElementById('sasuga-add-button').disabled = true;
	textboxes = 0;

	let tree = document.getElementById('sasuga-ua-tree');
	let index = addRow(domain, override);
	if (index >= 0) {
	    treebox.rowCountChanged(index, 1);
	}
	tree.focus();
    };

    o.activate = function (box) {
	switch (box.id) {
	case 'sasuga-domain-input':
	case 'sasuga-ua-input':
	    if (box.value.length > 0) {
		textboxes = textboxes + 1;
	    } else {
		textboxes = textboxes - 1;
	    }
	    break;
	default:
	    console.error(Strings.getFormattedString('invalidId', [box.id]));
	    return;
	}

	if (textboxes >= 2) {
	    document.getElementById('sasuga-add-button').disabled = false;
	} else {
	    document.getElementById('sasuga-add-button').disabled = true;
	}
    };

    o.select = function (tree) {
	let count = tree.view.selection.getRangeCount();
	if (count > 0) {
	    document.getElementById('sasuga-delete-button').disabled = false;
	} else {
	    document.getElementById('sasuga-delete-button').disabled = true;
	}
    };

    o.delete = function () {
	let tree = document.getElementById('sasuga-ua-tree');
	let count = tree.view.selection.getRangeCount();

	treebox.beginUpdateBatch();
	for (let i=count-1; i>=0; --i) {
	    let start = {};
	    let end = {};
	    tree.view.selection.getRangeAt(i, start, end);

	    for (let v=end.value; v>=start.value; --v) {
		let column =
		    treebox.columns.getNamedColumn('sasuga-domain-column');
		let prefName = tree.view.getCellText(v, column);

		// Just to make sure...
		if (branch.prefHasUserValue(prefName) === false) {
		    continue;
		}

		branch.clearUserPref(prefName);
		removeRow(v);
		treebox.rowCountChanged(v, -1);
	    }
	}
	treebox.endUpdateBatch();
	tree.focus();
    };

    o.dblclick = function (tree) {
	// Here we pick only the values from the first selected item
	// in the list.  No need to complicate the code with the
	// handling of multiple selected items.
	let start = {};
	let end = {};
	tree.view.selection.getRangeAt(0, start, end);

	let column;

	column = treebox.columns.getNamedColumn('sasuga-domain-column');
	let domain = tree.view.getCellText(start.value, column);

	column = treebox.columns.getNamedColumn('sasuga-ua-column');
	let ua = tree.view.getCellText(start.value, column);

	o.fillInput(domain, ua);
    };

    o.fillInput = function (domain, ua) {
	let domainBox = document.getElementById('sasuga-domain-input');
	let uaBox = document.getElementById('sasuga-ua-input');

	if (domain) {
	    domainBox.value = domain;
	    o.activate(domainBox);
	}

	if (ua) {
	    uaBox.value = ua;
	    o.activate(uaBox);
	}
    };

    o.onLoad = function () {
	let tnode =
	    document.createTextNode(Strings.getString('treeDescription'));
	document.getElementById('sasuga-tree-description').appendChild(tnode);

	o.fillInput(window.arguments[0].host);
    };

    return o;
})();
